package de.oth.dsrp.unit2;


import de.oth.dsrp.unit1.RandomGenerator;

import java.util.*;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Simulation {

    private final double[] nodesProbabilities;
    private final double[][] transitProbabilities;

    private boolean iterationMode;
    private String typeOfSimulation;

    public static Simulation startSimulation(double[][] stochasticMatrix, int iterationCount) {
        return new Simulation(stochasticMatrix, iterationCount);
    }

    public static Simulation startSimulation(double[][] stochasticMatrix, double errorDeviation) {
        return new Simulation(stochasticMatrix, errorDeviation);
    }

    private Simulation(int length) {
        this.nodesProbabilities = new double[length];
        this.transitProbabilities = new double[length][length];

    }

    private Simulation(double[][] stochasticMatrix, int iterations) {
        this(stochasticMatrix.length);
        this.iterationMode = true;
        this.typeOfSimulation = "iteration";
        simulation(stochasticMatrix, (count, deviation) -> count < iterations - 1);
    }

    private Simulation(double[][] stochasticMatrix, double errorDeviation) {
        this(stochasticMatrix.length);
        this.iterationMode = false;
        this.typeOfSimulation = "error deviation";
        simulation(stochasticMatrix, (count, deviation) -> deviation < errorDeviation);
    }

    private void simulation(double[][] stochasticMatrix, BiPredicate<Integer, Double> predicate) {

        int nodesCount = stochasticMatrix.length;
        int[][] transitHistory = new int[nodesCount][nodesCount];
        int[] nodeHistory = new int[nodesCount];
        List<Integer> classes = IntStream.rangeClosed(0, nodesCount - 1).boxed().collect(Collectors.toList());
        List<List<Double>> probabilities = new ArrayList<>(nodesCount);

        for(int i = 0; i < nodesCount; i++) {
            probabilities.add(Arrays.stream(stochasticMatrix[i]).mapToObj(Double::new).collect(Collectors.toList()));
        }

        int actualNode = 0, iterationCount = 0;

        do {
            List<Double> rowProbabilities = probabilities.get(actualNode);
            int nextTransit = (int)RandomGenerator.createCategoricalGenerator(classes, rowProbabilities).nextDouble();
            transitHistory[actualNode][nextTransit]++;
            nodeHistory[nextTransit]++;
            iterationCount++;
            actualNode = nextTransit;
        } while(checkErrorDeviation(stochasticMatrix, transitHistory, iterationCount, predicate));

        calculateProbabilities(nodeHistory, transitHistory, iterationCount);
    }

    private void calculateProbabilities(int[] nodes, int[][] transits, int iterations) {
        int nodesCount = nodes.length;
        for (int i = 0; i < nodesCount; i++) {
            nodesProbabilities[i] = (double) nodes[i] / (double) iterations;

            for (int j = 0; j < nodesCount; j++) {
                transitProbabilities[i][j] = (double) transits[i][j] / (double) (iterations / nodesCount);
            }
        }
    }

    private boolean checkErrorDeviation(double[][] stochasticMatrix, int[][] transits, int iterations, BiPredicate<Integer, Double> predicate) {
        boolean result = true;
        int nodes = stochasticMatrix.length;

        if (iterationMode) {
            return predicate.test(iterations, 0.0);
        }

        for (int i = 0; i < nodes; i++) {
            for (int j = 0; j < nodes; j++) {
                double divisor = (double)iterations / (double)nodes;
                double probability = transits[i][j] / divisor;
                double deviation = probability - stochasticMatrix[i][j];
                result &= predicate.test(iterations, Math.abs(deviation));
            }
        }

        return !result;
    }

    public double[] getNodesProbabilities() {
        return nodesProbabilities;
    }

    public double[][] getTransitProbabilities() {
        return transitProbabilities;
    }

    public String getTypeOfSimulation() {
        return typeOfSimulation;
    }
}
