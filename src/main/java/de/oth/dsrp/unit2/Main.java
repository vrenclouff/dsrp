package de.oth.dsrp.unit2;

import java.util.function.Supplier;

public class Main {

    private static final int ITERATIONS = 1000000;

    private static final double ERROR_DEVIATION = 0.001;

//    private static final double[][] STOCHASTIC_MATRIX = new double[][] {
//            { .2, .8 },
//            { .8, .2 },
//    };

    private static final double[][] STOCHASTIC_MATRIX = new double[][] {
            { .0, .6, .4 },
            { .6, .2, .2 },
            { .4, .2, .4 },
    };

    public static void main(String[] args) {

        printResult(() -> Simulation.startSimulation(STOCHASTIC_MATRIX, ITERATIONS));

        printResult(() -> Simulation.startSimulation(STOCHASTIC_MATRIX, ERROR_DEVIATION));
    }

    private static void printResult(Supplier<Simulation> runnable) {

        long startTime = System.currentTimeMillis();
        Simulation simulation = runnable.get();
        long estimatedTime = System.currentTimeMillis() - startTime;

        double[] result = simulation.getNodesProbabilities();
        double[][] transits = simulation.getTransitProbabilities();

        System.out.print(String.format("*** Markov's chain with %s simulation (%s ms) ***\n", simulation.getTypeOfSimulation(), estimatedTime));
        int nodeCount = result.length;

        System.out.print("calculated stochastic matrix\n\t");
        for (int i = 0; i < nodeCount; i++) {
            System.out.print((char)(65+i)+"\t\t");
        }
        System.out.print("\n");
        for (int i = 0; i < nodeCount; i++) {
            System.out.print((char)(65+i)+"\t");
            for (int j = 0; j < nodeCount; j++) {
                System.out.print(String.format("%.2f\t", transits[i][j]));
            }
            System.out.print("\n");
        }


        System.out.print("\nprobability of each nodes\n");
        for (int i = 0; i < nodeCount; i++) {
            System.out.print((char)(65+i)+"\t\t");
        }
        System.out.print("\n");
        for (int i = 0; i < nodeCount; i++ ) {
            System.out.print(String.format("%.2f\t", result[i]));
        }
        System.out.print("\n");
    }
}
