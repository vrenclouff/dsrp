package de.oth.dsrp.unit1;


import java.util.List;

public interface RandomGenerator {

    double nextDouble();

    double meanValue();
    double deviationValue();

    static RandomGenerator createCategoricalGenerator(List<Integer> values, List<Double> probabilities) {
        return new CategoricalRandomGenerator(values, probabilities);
    }

    static RandomGenerator createExponentialGenerator(int rate) {
        return new ExponentialRandomGenerator(rate);
    }
}
