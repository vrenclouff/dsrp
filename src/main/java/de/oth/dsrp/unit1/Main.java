package de.oth.dsrp.unit1;

import java.util.Arrays;
import java.util.List;

public class Main {

    private static List<Integer> values = Arrays.asList(1, 2, 3, 4);
    private static List<Double> probabilities = Arrays.asList(0.1, 0.2, 0.3, 0.4);

//    private static List<Integer> values = Arrays.asList(1, 2);
//    private static List<Double> probabilities = Arrays.asList(0.1, 0.9);

    private static int rate = 10;

    public static void main(String[] args) {

        RandomGenerator categoricalGen = RandomGenerator.createCategoricalGenerator(values, probabilities);
        printSimulationResult(Simulation.startSimulation(categoricalGen));

        RandomGenerator exponentialGen = RandomGenerator.createExponentialGenerator(rate);
        printSimulationResult(Simulation.startSimulation(exponentialGen));

    }

    public static void printSimulationResult(Simulation simulation) {
        System.out.println(String.format("***** SIMULATION of %s *****", simulation.getRandomGenerator().getClass().getSimpleName()));

        System.out.println(String.format("Expected mean:\t%.3f", simulation.getExpectedMean()));
        System.out.println(String.format("Calculated mean:\t%.3f", simulation.getCalculatedMean()));

        System.out.println(String.format("Expected deviation:\t%.3f", simulation.getExpectedDeviation()));
        System.out.println(String.format("Calculated deviation:\t%.3f\n", simulation.getCalculatedDeviation()));
    }
}
