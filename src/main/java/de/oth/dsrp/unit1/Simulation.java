package de.oth.dsrp.unit1;

public class Simulation {

    private static int ITERATIONS = 10000000;

    private RandomGenerator randomGenerator;

    private double calculatedMean, calculatedDeviation;

    public static Simulation startSimulation(RandomGenerator randomGenerator) {
        return new Simulation(randomGenerator);
    }

    private Simulation(RandomGenerator randomGenerator) {
        this.randomGenerator = randomGenerator;
        this.run();
    }

    private void run() {
        double mean = 0, variance = 0;
        double realMean = getExpectedMean();
        for (int i = 0; i < ITERATIONS; i++ ){
            double value = randomGenerator.nextDouble();
            mean += value;
            variance += Math.pow(value - realMean, 2);
        }

        calculatedMean = mean / ITERATIONS;
        calculatedDeviation = Math.sqrt(variance / ITERATIONS);
    }

    public double getExpectedMean() {
        return randomGenerator.meanValue();
    }

    public double getCalculatedMean() {
        return calculatedMean;
    }

    public double getExpectedDeviation() {
        return randomGenerator.deviationValue();
    }

    public double getCalculatedDeviation() {
        return calculatedDeviation;
    }

    public RandomGenerator getRandomGenerator() {
        return randomGenerator;
    }
}
