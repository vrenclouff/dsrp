package de.oth.dsrp.unit1;

import java.util.Random;

public class ExponentialRandomGenerator implements RandomGenerator {

    private final double rate;

    private final Random uniformGenerator = new Random();

    protected ExponentialRandomGenerator(int rate) {
        this.rate = rate;
    }

    private double generateValue() {
        double x = uniformGenerator.nextDouble();
        return Math.log(1-x)/(-rate);
    }

    @Override
    public double nextDouble() {
        return generateValue();
    }

    @Override
    public double meanValue() {
        return 1 / rate;
    }

    @Override
    public double deviationValue() {
        return 1 / rate;
    }

}
