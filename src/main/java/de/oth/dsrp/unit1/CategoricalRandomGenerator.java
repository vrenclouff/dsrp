package de.oth.dsrp.unit1;

import java.util.List;
import java.util.Random;


public class CategoricalRandomGenerator implements RandomGenerator {

    private final List<Double> probabilities;

    private final List<Integer> values;

    private final Random uniformGenerator = new Random();

    protected CategoricalRandomGenerator(List<Integer> values, List<Double> probabilities) {
        this.values = values;
        this.probabilities = probabilities;
    }

    private double generateValue() {

        if (values.size() != probabilities.size()) {
            throw new IllegalArgumentException("Values and they probabilities don't have the same length.");
        }

        if (probabilities.stream().mapToDouble(Double::new).sum() != 1) {
            throw new IllegalArgumentException("Hodnoty nejsou 1.");
        }

        double x = uniformGenerator.nextDouble();
        double boundary = 0;
        int index = -1;

        do {
            boundary += probabilities.get(++index);
        } while (boundary <= x);

        return values.get(index);
    }

    @Override
    public double nextDouble() {
        return generateValue();
    }

    @Override
    public double meanValue() {
        double sum = 0;
        for (int i = 0; i < values.size(); i++) {
            sum += values.get(i) * probabilities.get(i);
        }
        return sum;
    }

    @Override
    public double deviationValue() {
        double sum = 0;
        for (int i = 0; i < values.size(); i++) {
            sum += values.get(i) * probabilities.get(i) * (1 - probabilities.get(i));
        }
        return sum;
    }

}
